# Headers

A small utility to audit webserver headers. It will list all headers returned, indicate the risky headers and outline any missing security headers on the remote server.

## Usage

To audit a website simply invoke the application `headers` with the `--url` option to pass along the address of the website to test. The following example was done against a standard Python server. You can test this on your own computer using `python -m SimpleHTTPServer 8080

```
$ headers --url http://localhost:8080
Headers 0.1 by Arjen (@credmp) Wiersma

Auditing:  http://localhost:8080
HTTP Status Code: 200

Missing security headers:

X-Frame-Options: X-Frame-Options tells the browser whether you want to allow your site to be framed or not. By preventing a browser from framing your site you can defend against attacks like clickjacking.
X-Xss-Protection: X-Xss-Protection sets the configuration for the cross-site scripting filters built into most browsers. The best configuration is "X-XSS-Protection: 1; mode=block".
Content-Security-Policy: Content Security Policy is an effective measure to protect your site from XSS attacks. By whitelisting sources of approved content, you can prevent the browser from loading malicious assets.
Referrer-Policy: Referrer Policy is a new header that allows a site to control how much information the browser includes with navigations away from a document and should be set by all sites.
Feature-Policy: Feature Policy is a new header that allows a site to control which features and APIs can be used in the browser.
X-Content-Type-Options: X-Content-Type-Options stops a browser from trying to MIME-sniff the content type and forces it to stick with the declared content-type. The only valid value for this header is "X-Content-Type-Options: nosniff".
Expect-CT: Expect-CT allows a site to determine if they are ready for the upcoming Chrome requirements and/or enforce their CT policy.
Strict-Transport-Security: HTTP Strict Transport Security is an excellent feature to support on your site and strengthens your implementation of TLS by getting the User Agent to enforce the use of HTTPS.

Headers:

	    Content-Length: 3660
	[!] Server: SimpleHTTP/0.6 Python/2.7.10
	    Date: Sun, 30 Sep 2018 20:40:16 GMT
	    Content-Type: text/html; charset=utf-8
```

## Why Go?

Well why not? I wanted an excuse to try and build something useful for my day-to-day work.

# ROADMAP

I plan to add more HTTP Server auditing features as I need them. Output to [ReconJSON](https://github.com/ReconJSON/ReconJSON) is also in my planning.

Besides that there is not much of a roadmap, sorry.

# CONTRIBUTING

Any PR is always welcome. 

You might think the app is somewhat over-engineered. The reason for this is a presentation by Katzien at GopherCon ([Youtube](https://www.youtube.com/watch?v=oL6JBUk6tj0)). She presented a maintainable, self-explaining code layout for more complex applications.

As you see, this app is not very complex, but it was a good excuse to put the design to use.