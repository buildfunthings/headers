// Copyright © 2018 Arjen 'credmp' Wiersma <arjen@wiersma.org>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package audit

import (
	"crypto/tls"
	"fmt"
	"net/http"
)

// Outputter provides access to the reporting layer
type Outputter interface {
	AddHeader(header Header)
	AddMissingHeader(header HeaderMetaData)
	SetStatusCode(statuscode int)
	SetURL(URL string)
	// WriteReport will output the audit Result to the chose format
	WriteReport() error
}

// Auditor is the main audit component
type Auditor interface {
	Perform(url string, opts *Options) error
}

// Auditor holds the business logic to audit remote hosts
type auditor struct {
	o Outputter
}

// NewService creates a service to perform audits and outputs to Outputter
func NewService(o Outputter) Auditor {
	return &auditor{o}
}

func (a *auditor) Perform(url string, opts *Options) error {
	a.o.SetURL(url)

	client := &http.Client{}

	if !opts.Redirect {
		client.CheckRedirect = NoRedirect
	}

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: opts.InSecure}

	resp, err := client.Get(url)
	if err != nil {
		return fmt.Errorf("failed to retrieve %s error %s", url, err)
	}
	defer resp.Body.Close()

	// fmt.Println()
	// fmt.Printf("HTTP StatusCode: %d\n", au.Green(resp.StatusCode).Bold())
	// fmt.Println()
	a.o.SetStatusCode(resp.StatusCode)

	missing := findMissingSecurityHeaders(resp.Header)

	for _, m := range missing {
		a.o.AddMissingHeader(m)
	}

	for header := range resp.Header {
		meta, err := GetHeaderMetaData(header)

		if err != nil {
			a.o.AddHeader(Header{Key: header, Value: resp.Header.Get(header), Type: HtNormal})
		} else {
			a.o.AddHeader(Header{Key: header, Value: resp.Header.Get(header), Type: meta.Type})
		}
	}

	return nil
}

func NoRedirect(req *http.Request, via []*http.Request) error {
	return http.ErrUseLastResponse
}

func GetHeaderMetaData(key string) (HeaderMetaData, error) {
	for i := range HeaderMeta {
		if HeaderMeta[i].Key == key {
			return HeaderMeta[i], nil
		}
	}

	return HeaderMetaData{}, fmt.Errorf("unknown header: %s", key)
}

func getHeader(key string, headers http.Header) (string, error) {
	for k := range headers {
		if key == k {
			return key, nil
		}
	}

	return "", fmt.Errorf("could not find header %s", key)
}

func findMissingSecurityHeaders(headers http.Header) []HeaderMetaData {
	result := make([]HeaderMetaData, 0)

	// This is terribly inefficient, but a first attempt to sort out my mind
	for i := range HeaderMeta {
		_, err := getHeader(HeaderMeta[i].Key, headers)
		// If the header is not found, add it to the list
		if err != nil {
			result = append(result, HeaderMeta[i])
		}
	}

	return result
}
