// Copyright © 2018 Arjen 'credmp' Wiersma <arjen@wiersma.org>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package audit

// Options hold the application level options passed to other components
type Options struct {
	ShortOutput bool
	Redirect    bool
	InSecure    bool // do not check CA certificate
}

// HeaderType indicates the use for the header
type HeaderType int

const (
	HtNormal HeaderType = iota
	HtSecurity
	HtTLS
	HtRisk
)

// Header retrieved from a webserver, enriched with metadata
type Header struct {
	Key   string
	Value string
	Type  HeaderType
}

// HeaderMetaData which explains known HTTP Headers
type HeaderMetaData struct {
	Key         string
	Type        HeaderType
	Description string
}

// HeaderMeta contains known headers
var HeaderMeta = []HeaderMetaData{
	HeaderMetaData{
		Key:         "X-Frame-Options",
		Type:        HtSecurity,
		Description: "X-Frame-Options tells the browser whether you want to allow your site to be framed or not. By preventing a browser from framing your site you can defend against attacks like clickjacking.",
	},
	HeaderMetaData{
		Key:         "X-Xss-Protection",
		Type:        HtSecurity,
		Description: "X-Xss-Protection sets the configuration for the cross-site scripting filters built into most browsers. The best configuration is \"X-XSS-Protection: 1; mode=block\".",
	},
	HeaderMetaData{
		Key:         "Content-Security-Policy",
		Type:        HtSecurity,
		Description: "Content Security Policy is an effective measure to protect your site from XSS attacks. By whitelisting sources of approved content, you can prevent the browser from loading malicious assets.",
	},
	HeaderMetaData{
		Key:         "Referrer-Policy",
		Type:        HtSecurity,
		Description: "Referrer Policy is a new header that allows a site to control how much information the browser includes with navigations away from a document and should be set by all sites.",
	},
	HeaderMetaData{
		Key:         "Feature-Policy",
		Type:        HtSecurity,
		Description: "Feature Policy is a new header that allows a site to control which features and APIs can be used in the browser.",
	},
	HeaderMetaData{
		Key:         "X-Content-Type-Options",
		Type:        HtSecurity,
		Description: "X-Content-Type-Options stops a browser from trying to MIME-sniff the content type and forces it to stick with the declared content-type. The only valid value for this header is \"X-Content-Type-Options: nosniff\".",
	},
	HeaderMetaData{
		Key:         "Expect-CT",
		Type:        HtSecurity,
		Description: "Expect-CT allows a site to determine if they are ready for the upcoming Chrome requirements and/or enforce their CT policy.",
	},
	HeaderMetaData{
		Key:         "Strict-Transport-Security",
		Type:        HtSecurity,
		Description: "HTTP Strict Transport Security is an excellent feature to support on your site and strengthens your implementation of TLS by getting the User Agent to enforce the use of HTTPS.",
	},
	HeaderMetaData{
		Key:         "Server",
		Type:        HtRisk,
		Description: "This Server header seems to advertise the software being run on the server but you can remove or change this value.",
	},
	HeaderMetaData{
		Key:         "X-Powered-By",
		Type:        HtRisk,
		Description: "Used by frameworks to publicize that they are used, most of the time including version information.",
	},
	HeaderMetaData{
		Key:         "X-AspNet-Version",
		Type:        HtRisk,
		Description: "Indicates the exact version of ASP.NET in use at the site",
	},
	HeaderMetaData{
		Key:         "X-AspNetMvc-Version",
		Type:        HtRisk,
		Description: "Indicates the exact version of ASP.NET MVC in use at the site",
	},
	HeaderMetaData{
		Key:         "Set-Cookied",
		Type:        HtRisk,
		Description: "Contains cookies set by the site, can contain sensitive information and should be HttpOnly and Secure", // Idea: create validators that will check the value of the header?
	},
}
