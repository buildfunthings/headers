// Copyright © 2018 Arjen 'credmp' Wiersma <arjen@wiersma.org>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package console

import (
	"fmt"

	"gitlab.com/buildfunthings/headers/pkg/audit"
)

// Result is the result from the audit run on the target server
type Result struct {
	URL        string
	StatusCode int
	Missing    []audit.HeaderMetaData
	Current    []audit.Header
}

// AddMissingHeader populates the result object with Missing headers
func (r *Result) AddMissingHeader(header audit.HeaderMetaData) {
	r.Missing = append(r.Missing, header)
}

// AddHeader adds a header to the result struct
func (r *Result) AddHeader(header audit.Header) {
	r.Current = append(r.Current, header)
}

// SetStatusCode registers the status code returned by the server
func (r *Result) SetStatusCode(statuscode int) {
	r.StatusCode = statuscode
}

// SetURL adds the URL to the result object
func (r *Result) SetURL(URL string) {
	r.URL = URL
}

// WriteReport will output the audit Result to the chose format
func (r *Result) WriteReport() error {
	fmt.Printf("Auditing: %s (%d)\n", r.URL, r.StatusCode)
	fmt.Println()

	if len(r.Missing) > 0 {
		fmt.Println("Missing security headers:")
		fmt.Println()

		for i := range r.Missing {
			fmt.Printf("%s: %s\n", r.Missing[i].Key, r.Missing[i].Description)
			fmt.Println()
		}
		fmt.Println()
	}

	fmt.Printf("All Headers:\n")
	fmt.Println()

	for _, header := range r.Current {
		printHeader(header)
	}

	return nil
}

// NewResult creates a new Result struct
func NewResult() Result {
	return Result{}
}

func printHeader(header audit.Header) {
	// if val, err := GetHeaderMetaData(key); err == nil {
	if header.Type == audit.HtRisk {
		fmt.Printf("\t[!] %s: %s\n", header.Key, header.Value)
	} else if header.Type == audit.HtSecurity {
		fmt.Printf("\t[S] %s: %s\n", header.Key, header.Value)
	} else {
		fmt.Printf("\t    %s: %s\n", header.Key, header.Value)
	}
	// } else {
	// 	fmt.Printf("\t%s: %s\n", key, header.Get(key))
	// }
}
