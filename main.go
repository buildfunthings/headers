package main

import (
	"fmt"

	"gitlab.com/buildfunthings/headers/cmd"
)

const (
	// VERSION is the actual version of this program
	VERSION = "0.1"
)

func main() {
	fmt.Printf("Headers %s by Arjen (%s) Wiersma\n", VERSION, "@credmp")
	fmt.Println()

	// delegate immediately to cmd/root.go to handle the request
	cmd.Execute()
}
