// Copyright © 2018 Arjen 'credmp' Wiersma <arjen@wiersma.org>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/buildfunthings/headers/pkg/audit"
	"gitlab.com/buildfunthings/headers/pkg/output/console"
)

var cfgFile string

//var au aurora.Aurora

//var colors bool
var redirect bool
var url string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "headers",
	Short: "Audit HTTP Headers returned by a webserver",
	Long: `Audits the returned HTTP Headers for any given URL. It identifies risky
headers that expose sensitive information (such as versions or 
frameworks) and highlights security headers.

If a security header is missing it will tell you what the particular 
header is for.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: runRoot,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.headers.yaml)")

	rootCmd.Flags().StringVarP(&url, "url", "u", "", "URL to audit")
	rootCmd.Flags().BoolVarP(&redirect, "follow-redirect", "f", false, "follow redirects returned by the server")
	rootCmd.Flags().BoolP("insecure", "i", false, "do not verify the SSL certificate on the remote server")
	rootCmd.MarkFlagRequired("url")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".headers" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".headers")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func runRoot(cmd *cobra.Command, args []string) {
	result := &console.Result{}
	service := audit.NewService(result)
	opts := &audit.Options{}

	val, _ := cmd.Flags().GetBool("insecure")
	opts.InSecure = val
	// opts.ShortOutput = val // TODO: implement usage for this.

	opts.Redirect = redirect

	err := service.Perform(url, opts)
	if err != nil {
		fmt.Printf("Error while performing audit: %s", err)
		return
	}

	err = result.WriteReport()
	if err != nil {
		fmt.Println(result, err)
	}
}
